<?php
namespace gamepedia\controleur;

require 'vendor/autoload.php';

use gamepedia\modele\Company;
use gamepedia\modele\Game;
use gamepedia\modele\Game_rating;
use gamepedia\modele\Genre;
use gamepedia\modele\Platform;
use gamepedia\modele\Character;
use gamepedia\vue\VueRequete;

class ControleurRequete{


    public function afficher(){
        $vue = new VueRequete(['']);
        $vue->render(0);
    }

    public function requete1(){
        $games = Game::where('name', 'like', '%Mario%')->get();
        $vue = new VueRequete($games);
        $vue->render(1);
    }

    public function requete2(){
        $companys = Company::where('location_country', 'like', 'japan')->get();
        $vue = new VueRequete($companys);
        $vue->render(2);
    }

    public function requete3(){
        $platforms = Platform::where('install_base', '>=', '10000000')->get();
        $vue = new VueRequete($platforms);
        $vue->render(3);
    }

    public function requete4(){
        $games = Game::take(442)->skip(21172)->get();
        $vue = new VueRequete($games);
        $vue->render(4);
    }

    public function requete5($page){
        $games = Game::take(500)->skip(($page - 1) * 500)->get();
        $games['page'] = $page;
        $vue = new VueRequete($games);
        $vue->render(5);
    }

    public function requete6(){
        $personnages = Character::whereHas('games', function($q){
            $q->where('id', '=', '12342');
        })->get();
        $vue = new VueRequete($personnages);
        $vue->render(6);
    }

    public function requete7(){
        $personnages = Character::whereHas('games', function($q){
            $q->where('name', 'like', 'Mario%');
        })->get();
        $vue = new VueRequete($personnages);
        $vue->render(7);
    }

    public function requete8(){
        $games = Game::whereHas('devlopers', function ($q){
            $q->where('name', 'like', '%sony%');
        })->get();

        $vue = new VueRequete($games);
        $vue->render(8);
    }

    public function requete9(){
        $rating = Game_rating::whereHas('games', function ($q){
            $q->where('name', 'like', 'Mario%');
        })->get();
        $vue = new VueRequete($rating);
        $vue->render(9);
    }

    public function requete10(){
        $games = Game::where('name', 'like', 'Mario%')->has('characters', '>', '3')->take(1)->get();
        $vue = new VueRequete($games);
        $vue->render(10);
    }

    public function requete11(){
        $games =Game::where('name', 'like', 'Mario%')->whereHas('ratings', function ($q){
            $q->where('name', 'like', '3+');
        })->get();
        $vue = new VueRequete($games);
        $vue->render(11);
    }

    public function requete12(){
        $games =Game::where('name', 'like', 'Mario%')->whereHas('publishers', function ($q){
            $q->where('name', 'like', '%Inc%');
        })->whereHas('ratings', function ($q){
            $q->where('name', 'like', '3+');
        })->get();
        $vue = new VueRequete($games);
        $vue->render(12);
    }

    public function requete13(){
        $games =Game::where('name', 'like', 'Mario%')->whereHas('publishers', function ($q){
            $q->where('name', 'like', '%Inc%');
        })->whereHas('ratings', function ($q){
            $q->where('name', 'like', '3+');
        })->whereHas('ratings', function ($q){
            $q->where('name', 'like', 'CERO');
        })->get();
        $vue = new VueRequete($games);
        $vue->render(13);
    }

    public function requete14(){
        //ajouter un nouveau genre de jeu, et l'associer aux jeux 12, 56, 12, 345
        $nvGenre = new Genre();
        $nvGenre->name = 'nvGenre';
        $nvGenre->deck = 'deck nvGenre';
        $nvGenre->description = 'description nvGenre';
        $nvGenre->save();
        $nvGenre->games()->attach([12, 56, 345]);
    }

    public function tpRequete1(){
        $t = new TemsEx();
        $games = Game::take(15000)->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(14);
    }

    public function tpRequete2(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', '%Mario%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(14);
    }

    public function tpRequete3(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', 'Mario%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(14);
    }

    public function tpRequete4(){
        $t = new TemsEx();
        $games =Game::where('name', 'like', 'Mario%')->whereHas('ratings', function ($q){
            $q->where('name', 'like', '3+');
        })->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(14);
    }

    public function log($log){
        $vue = new VueRequete($log);
        $vue->render(15);
    }

    public function indexSeance3a(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', 'Mario%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(17);
    }

    public function indexSeance3b(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', 'Super%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(17);
    }

    public function indexSeance3c(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', 'G%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(17);
    }

    public function testcontient1(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', '%Mario%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(16);
    }

    public function testcontient2(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', '%Tetris%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(16);
    }

    public function testcontient3(){
        $t = new TemsEx();
        $games = Game::where('name', 'like', '%World%')->get();
        $res = [$t->tempsEx()];
        $vue = new VueRequete($res);
        $vue->render(16);
    }

    public function collectionJeu(){
        $game = Game::get();
        $vue = new VueRequete($game);
        $vue->render(19);
    }
    public function jeu($id){
        $jeu = Game::where('id', '=', $id)->first();
        $vue = new VueRequete($jeu);
        $vue->render(18);
    }
}