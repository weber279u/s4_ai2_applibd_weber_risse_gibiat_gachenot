<?php

namespace gamepedia\controleur;
require 'vendor/autoload.php';
class TemsEx
{
    private $tempsDebut;

    public function __construct(){
        $this->tempsDebut = microtime(true);
    }

    public function tempsEx()
    {
        return microtime(true) - $this->tempsDebut;
    }
}