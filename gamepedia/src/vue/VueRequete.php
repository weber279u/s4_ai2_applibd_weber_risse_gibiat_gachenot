<?php
namespace gamepedia\vue;


require 'vendor/autoload.php';

class VueRequete{

    private $t;
    private $r;

    public function __construct($t){
        $this->t = $t;
        $app = \Slim\Slim::getInstance();
        $this->r = $app->request->getRootUri();
    }


    public function afficherJeu(){
        $this->t->description = strip_tags($this->t->description);
        $pretty = json_encode($this->t, JSON_PRETTY_PRINT);
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        echo $pretty;
    }

    public function afficherCollection(){
        foreach ($this->t as $jeu){
            $jeu->description = strip_tags($jeu->description);
        }
        $pretty = json_encode($this->t, JSON_PRETTY_PRINT);
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        echo $pretty;
    }

    public function afficher(){
    }

    public function afficherR1(){
        $res = '<h1>
                    lister les jeux dont le nom contient \'Mario\'
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom jeu</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $game){
            $res .= '<tr><td>'. $game->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }
    public function afficherR2(){
        $res = '<h1>
                    lister les compagnies installées au Japon
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom company</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $company){
            $res .= '<tr><td>'. $company->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR3(){
        $res = '<h1>
                    lister les plateformes dont la base installée est >= 10 000 000
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom platforme</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $platform){
            $res .= '<tr><td>'. $platform->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR4(){
        $res = '<h1>
                   lister 442 jeux à partir du 21173ème
                </h1>
                <table>
					<thead>
						<tr>
							<th>id jeu</th>
							<th>nom jeu</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $game){
            $res .= '<tr><td>'. $game->id .'</td>
                     <td>'. $game->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR5(){
        $page = $this->t['page'];
        unset($this->t['page']);
        $res = '<h1>
                    lister les jeux, afficher leur nom et deck, en paginant (taille des pages : 500)
                </h1>
                <ul>
                <table>
					<thead>
						<tr>
							<th>nom jeu</th>
							<th>deck jeu</th>
						</tr >
					</thead >
					<tbody > ';

        foreach ($this->t as $game){
            $res .= '<tr><td>'. $game->name .'</td>
                     <td>' .$game->deck .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';

        $res .='<div class="pagination">';
            if ($page >= 2){
                $res .= '<a href="'. $this->r.'/r5/'. ($page - 1) .'">&laquo;</a>';
            }
        if ($page >= 2){
            $res .= '<a href="'. $this->r.'/r5/'. ($page - 1) .'">'. ($page - 1) .'</a>
                  <a href="'. $this->r.'/r5/'.$page.'" class="active">'.$page.'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">'. ($page + 1).'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 2) .'">'. ($page + 2) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 3) .'">'. ($page + 3) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 4) .'">'. ($page + 4) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">&raquo;</a>
                </div>';
        }elseif ($page >= 3){
                $res .= '<a href="'. $this->r.'/r5/'. ($page - 2) .'">'. ($page - 2) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page - 1) .'">'. ($page - 1).'</a>
                  <a href="'. $this->r.'/r5/'.$page.'" class="active">'.$page.'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">'. ($page + 1) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 2) .'">'. ($page + 2) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 3) .'">'. ($page + 3) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">&raquo;</a>
                </div>';
        }elseif ($page >= 4){
                $res .= '<a href="'. $this->r.'/r5/'. ($page - 3) .'">'. ($page - 3) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page - 2) .'">'. ($page - 2).'</a>
                  <a href="'. $this->r.'/r5/'. ($page - 1) .'">'. ($page - 1) .'</a>
                  <a href="'. $this->r.'/r5/'.$page.'" class="active">'.$page.'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">'. ($page + 1) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 2) .'">'. ($page + 2) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">&raquo;</a>
                </div>';
        }else{
                  $res .= '<a href="'. $this->r.'/r5/'.$page.'" class="active">'.$page.'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">'. ($page + 1) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 2) .'">'. ($page + 2).'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 3) .'">'. ($page + 3) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 4) .'">'. ($page + 4) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 5) .'">'. ($page + 5) .'</a>
                  <a href="'. $this->r.'/r5/'. ($page + 1) .'">&raquo;</a>
                </div>';
            }
        return $res;
    }

    public function afficherR6()
    {
        $res = '<h1>
                    Afficher les personnages du jeu 12342
                    </h1>
                    <ul>';
        foreach ($this->t as $character) {
            $res .= '<li>Nom du personnage :' . $character->name . 'Deck du personnage :' . $character->deck . '</li>';
        }
        $res .= '</ul>';
        return $res;
    }

    public function afficherR7(){
        $res = '<h1>
                    les personnages des jeux dont le nom (du jeu) débute par \'Mario\'
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom personnage</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $char){
            $res .= '<tr><td>'.$char->name .'</td></tr>';
//                        <td>'.  .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR8(){
        $res = '<h1>
                    les jeux développés par une compagnie dont le nom contient Sony
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom jeu</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $game){
            $res .= '<tr><td>'.$game->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR9(){
        $res = '<h1>
                    le rating initial (indiquer le rating board) des jeux dont le nom contient Mario
                </h1>
                <table>
					<thead>
						<tr>
							<th>rang</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $rating){
            $res .= '<tr><td>'. $rating->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR10(){
        $res = '<h1>
                    les jeux dont le nom débute par Mario et ayant plus de 3 personnages
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom jeu</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $game){
            $res .= '<tr><td>'. $game->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR11(){
        $res = '<h1>
                    les jeux dont le nom débute par Mario et dont le rating initial contient "3+"
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom jeu</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $game){
            $res .= '<tr><td>'. $game->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR12(){
        $res = '<h1>
                    • les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient
"Inc." et dont le rating initial contient "3+"

                </h1>
                <table>
					<thead>
						<tr>
							<th>nom jeu</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $game){
            $res .= '<tr><td>'. $game->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR13(){
        $res = '<h1>
                    les jeux dont le nom débute Mario, publiés par une compagnie dont le nom contient "Inc",
dont le rating initial contient "3+" et ayant reçu un avis de la part du rating board nommé
"CERO"
                </h1>
                <table>
					<thead>
						<tr>
							<th>nom jeu</th>
						</tr >
					</thead >
					<tbody >';

        foreach ($this->t as $game){
            $res .= '<tr><td>'. $game->name .'</td></tr>';
        }
        $res .= '</tbody>
				</table>';
        return $res;
    }

    public function afficherR14(){
        $res = '<h1>
                    temps execution : '. $this->t[0] .' seconde
                </h1>';
        return $res;
    }

    public function afficherLog()
    {
        var_dump($this->t);
        $res = '';
    }

    public function afficherIndexSeance3(){
        $res = '<h1>
                    <ul><li><a href="test">Test1</a></li>
                    <li><a href="test2">Test2</a></li>
                    <li><a href="test3">Test3</a></li>
                    </ul>
                    </h1>';
        foreach($this->t as $tempsEx){
            $res .= 'Temps d\'execution = ' . $this->t[0] . 'secondes';
        }
        return $res;
    }

    public function afficherIndexContient(){
        $res = '<h1>
                    <ul><li><a href="testcontient1">Test1</a></li>
                    <li><a href="testcontient2">Test2</a></li>
                    <li><a href="testcontient3">Test3</a></li>
                    </ul>
                    </h1>';
        foreach($this->t as $tempsEx){
            $res .= 'Temps d\'execution = ' . $this->t[0] . 'secondes';
        }

        return $res;
    }

    public function render(int $selecteur) {
        $content = '';
        switch ($selecteur) {
            case 0 :
                $content = $this->afficher();
                break;

            case 1 :
                $content = $this->afficherR1();
                break;

            case 2 :
                $content = $this->afficherR2();
                break;
            case 3 :
                $content = $this->afficherR3();
                break;
            case 4:
                $content = $this->afficherR4();
                break;
            case 5:
                $content = $this->afficherR5();
                break;
            case 6:
                $content = $this->afficherR6();
                break;
            case 7:
                $content = $this->afficherR7();
                break;
            case 8:
                $content = $this->afficherR8();
                break;

            case 9:
                $content = $this->afficherR9();
                break;

            case 10:
                $content = $this->afficherR10();
                break;

            case 11:
                $content = $this->afficherR11();
                break;

            case 12:
                $content = $this->afficherR12();
                break;

            case 13:
                $content = $this->afficherR13();
                break;

            case 14:
                $content = $this->afficherR14();
                break;

            case 15:
                $content = $this->afficherLog();
                break;

            case 16:
                $content = $this->afficherIndexContient();
                break;

            case 17:
                $content = $this->afficherIndexSeance3();
                break;

        }
        $html = <<<END
        <!DOCTYPE html>
        <html>
            <head>
                <style>
                    .nav {
                      list-style-type: none;
                      margin: 0;
                      padding: 0;
                      overflow: hidden;
                      background-color: #333;
                    }
                    
                    .lin {
                      float: left;
                    }
                    
                    .lin a {
                      display: block;
                      color: white;
                      text-align: center;
                      padding: 14px 16px;
                      text-decoration: none;
                    }
                    
                    .lin a:hover {
                      background-color: #111;
                    }
                    .pagination {
                      display: inline-block;
                    }
                    
                    .pagination a {
                      color: black;
                      float: left;
                      padding: 8px 16px;
                      text-decoration: none;
                      transition: background-color .3s;
                      border: 1px solid #ddd;
                      margin: 0 4px;
                    }
                    
                    .pagination a.active {
                      background-color: #4CAF50;
                      color: white;
                      border: 1px solid #4CAF50;
                    }
                </style>
            </head>
            <body>
                <ul class="nav">
                  <li class="lin"><a href="$this->r">Home</a></li>
                  <li class="lin"><a href="$this->r/td1">td 1</a></li>
                  <li class="lin"><a href="$this->r/td2">td 2</a></li>
                  <li class="lin"><a href="$this->r/td3">td 3</a></li>
                  <li class="lin"><a href="$this->r/td4">td 4</a></li>
                  <li class="lin"><a href="$this->r/td5">td 5</a></li>
                  <li class="lin"><a href="$this->r/r6">requete 6</a></li>
                  <li class="lin"><a href="$this->r/r7">requete 7</a></li>
                  <li class="lin"><a href="$this->r/r8">requete 8</a></li>
                  <li class="lin"><a href="$this->r/r9">requete 9</a></li>
                  <li class="lin"><a href="$this->r/r10">requete 10</a></li>
                  <li class="lin"><a href="$this->r/r11">requete 11</a></li>
                  <li class="lin"><a href="$this->r/r12">requete 12</a></li>
                  <li class="lin"><a href="$this->r/r13">requete 13</a></li>
                  <li class="lin"><a href="$this->r/r14">requete 14</a></li>
                  <li class="lin"><a href="$this->r/test">Test</a></li>
                  <li class="lin"><a href="$this->r/testcontient1">TestLister</a></li>
                </ul>
                 $content
                 
            </body>
        <html>
END;
        echo $html;
    }
}