<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Game extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function firstCharacters (){
        return $this->hasMany('gamepedia\modele\Character', 'first_appeared_in_game_id');
    }

    public function characters(){
        return $this->belongsToMany('gamepedia\modele\Character', 'game2character', 'game_id', 'character_id');
    }

    public function publishers(){
        return $this->belongsToMany('gamepedia\modele\Company', 'game_publishers', 'game_id', 'comp_id');
    }

    public function devlopers(){
        return $this->belongsToMany('gamepedia\modele\Company', 'game_developers', 'game_id', 'comp_id');
    }

    public function genres(){
        return $this->belongsToMany('gamepedia\modele\Genre', 'game2genre', 'game_id', 'genre_id');
    }

    public function platforms(){
        return $this->belongsToMany('gamepedia\modele\Platform', 'game2platform', 'game_id', 'platform_id');
    }

    public function themes(){
        return $this->belongsToMany('gamepedia\modele\Theme', 'game2platform', 'game_id', 'theme_id');
    }

    public function similarGames(){
        return $this->belongsToMany('gamepedia\modele\Game', 'similar_games', 'game1_id', 'game2_id');
    }

    public function ratings(){
        return $this->belongsToMany('gamepedia\modele\Game_rating', 'game2rating', 'game_id', 'rating_id');
    }



}


?>