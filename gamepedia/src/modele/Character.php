<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Character extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'character';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function firstGame(){
        return $this->belongsTo('gamepedia\modele\Game', 'first_appeared_in_game_id');
    }

    public function games(){
        return $this->belongsToMany('gamepedia\modele\Game', 'game2character', 'character_id', 'game_id');
    }

    public function enemies(){
        return $this->belongsToMany('gamepedia\modele\Character', 'enemies', 'char1_id', 'char2_id');
    }

    public function friends(){
        return $this->belongsToMany('gamepedia\modele\Character', 'friends', 'char1_id', 'char2_id');
    }

}


?>