<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Theme extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'theme';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function games(){
        return $this->belongsToMany('gamepedia\modele\Game', 'game2platform', 'theme_id', 'game_id');
    }

}


?>