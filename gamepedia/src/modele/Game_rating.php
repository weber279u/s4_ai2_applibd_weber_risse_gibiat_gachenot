<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Game_rating extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'game_rating';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function ratingBoard(){
        return $this->belongsTo('gamepedia\modele\Rating_board', 'rating_board_id');
    }

    public function games(){
        return $this->belongsToMany('gamepedia\modele\Game', 'game2rating', 'rating_id', 'game_id');
    }


}


?>