<?php
/**
 * Created by PhpStorm.
 * User: Pouyoupy
 * Date: 01/04/2019
 * Time: 00:03
 */

namespace gamepedia\modele;


class Commentaire extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'commentaire';
    protected $primaryKey = 'id';
    public $timestamps = true;

    static function addCom($titre,$contenu,$dateCreation){
        $c1= new Commentaire();
        $c1->titre = $titre;
        $c1->contenu =$contenu ;
        $c1->dateCreation=$dateCreation;
        $maxJeu = rand(1,Game::max("id"));
        $maxUtilisateur = rand(1,Utilisateur::max("id"));
        $c1->id_utilisateur=$maxUtilisateur;
        $c1->id_game=$maxJeu;
        $c1->save();
    }

    public function commentaires() {
        return $this->hasMany('\gamepedia\Model\Commentaire','id_utilisateur');
    }


}