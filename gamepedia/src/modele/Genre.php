<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Genre extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'genre';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function games(){
        return $this->belongsToMany('gamepedia\modele\Game', 'game2genre', 'genre_id', 'game_id');
    }

}


?>