<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Platform extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'platform';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function company(){
        return $this->belongsTo('gamepedia\modele\Company', 'c_id');
    }

    public function games(){
        return $this->belongsToMany('gamepedia\modele\Game', 'game2platform', 'platform_id', 'game_id');
    }

}


?>