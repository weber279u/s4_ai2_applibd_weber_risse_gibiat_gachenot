<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Company extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'company';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function platforms(){
        return $this->hasMany('gamepedia\modele\Platform', 'c_id');
    }

    public function gamesPublish(){
        return $this->belongsToMany('gamepedia\modele\Game', 'game_publishers', 'comp_id', 'game_id');
    }

    public function gamesDevlop(){
        return $this->belongsToMany('gamepedia\modele\Game', 'game_developers', 'comp_id', 'game_id');
    }

}


?>