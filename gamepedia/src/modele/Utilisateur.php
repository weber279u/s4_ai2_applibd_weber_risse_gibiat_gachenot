<?php
/**
 * Created by PhpStorm.
 * User: Pouyoupy
 * Date: 01/04/2019
 * Time: 00:02
 */

namespace gamepedia\modele;


class Utilisateur extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = false;

    static function adduser($nom,$prenom,$email,$adresse,$numtel,$datenaiss){
        $user=new Utilisateur();
        $user->nom=$nom;
        $user->prenom=$prenom;
        $user->email=$email;
        $user->adresse=$adresse;
        $user->numtel=$numtel;
        $user->datenaiss=$datenaiss;
        $user->save();
    }

    public function commentaires() {
        return $this->hasMany('\gamepedia\Model\Commentaire','id_utilisateur');
    }
}