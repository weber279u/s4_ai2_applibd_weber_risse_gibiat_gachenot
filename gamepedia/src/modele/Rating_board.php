<?php
namespace gamepedia\modele;
require 'vendor/autoload.php';
class Rating_board extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'rating_board';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function ratingBoard(){
        return $this->hasMany('gamepedia\modele\Game_rating', 'rating_board_id');
    }

}


?>