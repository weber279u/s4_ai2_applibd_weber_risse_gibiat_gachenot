<?php
require_once __DIR__ . '/vendor/autoload.php';

use gamepedia\controleur\ControleurRequete;
use gamepedia\controleur\ControleurTD1;
use gamepedia\controleur\ControleurTD2;
use gamepedia\controleur\ControleurTD3;
use gamepedia\controleur\ControleurTD4;
use gamepedia\controleur\ControleurTD5;
use \Illuminate\Database\Capsule\Manager as DB;

session_start();

$db = new DB();
$db->addConnection(parse_ini_file(join(DIRECTORY_SEPARATOR, ['src', 'conf', 'conf.ini'])));
$db->setAsGlobal();
$db->bootEloquent();
$db->connection()->enableQueryLog();

$app =new \Slim\Slim();

if (!isset($_SESSION['racine'])){
    $app = \Slim\Slim::getInstance();

    $_SESSION['racine'] = $app->request->getRootUri().'/';

}


$app->get('/',function(){
    $c = new ControleurRequete();
    $c->afficher();
});

$app->get('/r1',function(){
    $c = new ControleurRequete();
    $c->requete1();
    print_r(DB::getQueryLog());
});

$app->get('/r2',function(){
    $c = new ControleurRequete();
    $c->requete2();
    print_r(DB::getQueryLog());
});

$app->get('/r3',function(){
    $c = new ControleurRequete();
    $c->requete3();
    print_r(DB::getQueryLog());
});

$app->get('/r4',function(){
    $c = new ControleurRequete();
    $c->requete4();
    print_r(DB::getQueryLog());
});

$app->get('/r5/:page',function($page){
    $c = new ControleurRequete();
    $c->requete5($page);
    print_r(DB::getQueryLog());
});

$app->get('/r6',function() {
    $c = new ControleurRequete();
    $c->requete6();
    print_r(DB::getQueryLog());
});

$app->get('/r7',function() {
    $c = new ControleurRequete();
    $c->requete7();
    print_r(DB::getQueryLog());
});

$app->get('/r8',function(){
    $c = new ControleurRequete();
    $c->requete8();
    print_r(DB::getQueryLog());
});

$app->get('/r9',function(){
    $c = new ControleurRequete();
    $c->requete9();
    print_r(DB::getQueryLog());
});

$app->get('/r10',function(){
    $c = new ControleurRequete();
    $c->requete10();
    print_r(DB::getQueryLog());
});


$app->get('/r11',function(){
    $c = new ControleurRequete();
    $c->requete11();
    print_r(DB::getQueryLog());
});


$app->get('/r12',function(){
    $c = new ControleurRequete();
    $c->requete12();
    print_r(DB::getQueryLog());
});


$app->get('/r13',function(){
    $c = new ControleurRequete();
    $c->requete13();
    print_r(DB::getQueryLog());
});

$app->get('/r14',function(){
    $c = new ControleurRequete();
    $c->requete14();
    print_r(DB::getQueryLog());
});

$app->get('/tpEx1',function(){
    $c = new ControleurRequete();
    $c->tpRequete1();
});

$app->get('/tpEx2',function(){
    $c = new ControleurRequete();
    $c->tpRequete2();
    print_r(DB::getQueryLog());
});

$app->get('/tpEx3',function(){
    $c = new ControleurRequete();
    $c->tpRequete3();
    print_r(DB::getQueryLog());
});

$app->get('/tpEx4',function(){
    $c = new ControleurRequete();
    $c->tpRequete4();
    print_r(DB::getQueryLog());
});

$app->get('/test', function(){
    $c = new ControleurRequete();
    $c->indexSeance3a();
    print_r(DB::getQueryLog());
});

$app->get('/test2', function(){
    $c = new ControleurRequete();
    $c->indexSeance3b();
    print_r(DB::getQueryLog());
});

$app->get('/test3', function(){
    $c = new ControleurRequete();
    $c->indexSeance3c();
    print_r(DB::getQueryLog());
});

$app->get('/testcontient1', function(){
    $c = new ControleurRequete();
    $c->testcontient1();
    print_r(DB::getQueryLog());
});

$app->get('/testcontient2', function(){
    $c = new ControleurRequete();
    $c->testcontient2();
    print_r(DB::getQueryLog());
});

$app->get('/testcontient3', function(){
    $c = new ControleurRequete();
    $c->testcontient3();
    print_r(DB::getQueryLog());
});

$app->get('/api/games', function(){
    $c = new ControleurRequete();
    $c->collectionJeu();
});

$app->get('/api/games/:id',function($id){
    $c = new ControleurRequete();
    $c->jeu($id);
});


$app->run();

session_destroy();

?>