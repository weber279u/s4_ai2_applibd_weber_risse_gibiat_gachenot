<?php

require 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use \gamepedia\modele\Utilisateur;
use \gamepedia\modele\Commentaire;
$db = new DB();
$db->addConnection(parse_ini_file(join(DIRECTORY_SEPARATOR, ['src', 'conf', 'conf.ini'])));
$db->setAsGlobal();
$db->bootEloquent();
$faker  =  Faker \ Factory :: create ( ' fr_FR ' );

for ($i=0; $i < 25000; $i++) {
    Utilisateur::adduser($faker->firstName ,$faker->lastName,$faker->email,$faker->streetAddress." ".$faker->city." ".$faker->postcode." ".$faker->state, $faker->phoneNumber,$faker->dateTimeThisCentury->format('Y-m-d'));
}


for($j=0; $j<250000; $j++){
    Commentaire::addCom($faker->text(10),$faker->text(127),$faker->date());
}


$u=Utilisateur::inRandomOrder()->first();
echo $u->nom. " " . $u->prenom;

$commentaires = $u->commentaires()->orderBy(dateCreation);
foreach ($commentaires as $com) {
    echo $com->titre."\n" . $com->dateCreation;
}