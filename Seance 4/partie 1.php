<?php

require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \gamepedia\modele\Utilisateur;

$db = new DB();
$db->addConnection(parse_ini_file(join(DIRECTORY_SEPARATOR, ['src', 'conf', 'conf.ini'])));
$db->setAsGlobal();
$db->bootEloquent();

$u1=new Utilisateur();
$u1->nom="Nom1";
$u1->prenom='prenon1';
$u1->email='email1@email.com';
$u1->adresse='adresse1';
$u1->numtel='1111111111';
$u1->datenaiss=date('2001-01-01');
$u1->save();

$u2=new Utilisateur();
$u2->nom="Nom2";
$u2->prenom='prenom2';
$u2->email='email2@email.com';
$u2->adresse='adresse2';
$u2->numtel='2222222222';
$u2->datenaiss=date('2002-02-02');
$u2->save();


$u3=new Utilisateur();
$u3->nom="Nom3";
$u3->prenom='prenom3';
$u3->email='email3@email.com';
$u3->adresse='adresse3';
$u3->numtel='3333333333';
$u3->datenaiss=date('2003-03-03');
$u3->save();