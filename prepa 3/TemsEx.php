<?php
/**
 * Created by PhpStorm.
 * User: Pouyoupy
 * Date: 13/03/2019
 * Time: 08:39
 */
public class TemsEx
{
    private $tempsDebut;

    public function __construct(){
        $this->tempsDebut = microtime(true);
    }

    public function tempsEx()
    {
        return microtime(true) - $this->tempsDebut;
    }
}